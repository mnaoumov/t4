﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EnvDTE;
using Microsoft.Build.Evaluation;
using Microsoft.VisualStudio.TemplateWizard;
using EnvProject = EnvDTE.Project;
using Project = Microsoft.Build.Evaluation.Project;
using ProjectItem = EnvDTE.ProjectItem;

namespace T4RuntimeTemplateWithCodeBehind.Wizard
{
    public class T4ProjectFixWizard : IWizard
    {
        DTE _env;
        string _templateFileName;
        EnvProject _envProject;

        public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            _env = automationObject as DTE;
        }

        public void ProjectFinishedGenerating(EnvProject project)
        {
        }

        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }

        public void BeforeOpeningFile(ProjectItem projectItem)
        {
        }

        public void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
            var fileName = projectItem.FileNames[0];
            if (fileName.EndsWith(".tt", StringComparison.InvariantCultureIgnoreCase))
            {
                _templateFileName = fileName;
                _envProject = projectItem.ContainingProject;
            }
        }

        public void RunFinished()
        {
            var projectFileName = _envProject.FileName;
            var projectName = _envProject.Name;
            UnloadProject(projectName);
            FixCsprojFile(projectFileName, _templateFileName);
            ReloadProject(projectName);
            var envTemplateFileItem = _env.Solution.FindProjectItem(_templateFileName);
            if (envTemplateFileItem != null)
            {
                envTemplateFileItem.Open();
                envTemplateFileItem.Document.Activate();
            }
        }

        void ReloadProject(string projectName)
        {
            SelectProject(projectName);
            _env.ExecuteCommand("Project.ReloadProject");
        }

        void UnloadProject(string projectName)
        {
            SelectProject(projectName);

            _env.ExecuteCommand("File.SaveAll");
            _env.ExecuteCommand("Project.UnloadProject");
        }

        void SelectProject(string projectName)
        {
            var solutionName = _env.Solution.Properties.Item("Name").Value.ToString();
            var projectPath = Path.Combine(solutionName, projectName);
            var solutionExplorer = _env.Windows.Item(Constants.vsWindowKindSolutionExplorer);
            solutionExplorer.Activate();
            var solutionHierarchy = (UIHierarchy) solutionExplorer.Object;
            var projectItem = solutionHierarchy.GetItem(projectPath);
            projectItem.Select(vsUISelectionType.vsUISelectionTypeSelect);
        }


        static void FixCsprojFile(string projectFileName, string templateFileName)
        {
            var templateShortName = Path.GetFileName(templateFileName);
            var templateRelativeFileName = MakeRelativePath(projectFileName, templateFileName);

            var project = new Project(projectFileName);

            foreach (var itemName in new[] { templateRelativeFileName, ReplaceTrailing(templateRelativeFileName), templateRelativeFileName + ".cs", templateRelativeFileName + ".designer.cs" })
            {
                foreach (var itemToRemove in project.GetItemsByEvaluatedInclude(itemName).ToArray())
                    project.RemoveItem(itemToRemove);
            }

            var itemGroup = project.Xml.AddItemGroup();
            itemGroup.AddItem("None", templateRelativeFileName,
                              new Dictionary<string, string>
                                  {
                                      { "Generator", "TextTemplatingFilePreprocessor" },
                                      { "LastGenOutput", templateShortName + ".designer.cs" },
                                  });
            itemGroup.AddItem("Compile", templateRelativeFileName + ".designer.cs",
                              new Dictionary<string, string>
                                  {
                                      { "AutoGen", "True" },
                                      { "DesignTime", "True" },
                                      { "DependentUpon", templateShortName },
                                  });
            itemGroup.AddItem("Compile", templateRelativeFileName + ".cs",
                              new Dictionary<string, string>
                                  {
                                      { "SubType", "Code" },
                                      { "DependentUpon", templateShortName }
                                  });

            project.Save();
            ProjectCollection.GlobalProjectCollection.UnloadProject(project);

            var wrongCodeBehindName = templateFileName.Replace(".tt", ".cs");
            File.Delete(wrongCodeBehindName);
        }

        static string ReplaceTrailing(string templateRelativeFileName)
        {
            return templateRelativeFileName.Replace(".tt", ".cs");
        }

        static string MakeRelativePath(string fromPath, string toPath)
        {
            if (string.IsNullOrEmpty(fromPath))
                throw new ArgumentNullException("fromPath");
            if (string.IsNullOrEmpty(toPath))
                throw new ArgumentNullException("toPath");

            var fromUri = new Uri(fromPath);
            var toUri = new Uri(toPath);

            var relativeUri = fromUri.MakeRelativeUri(toUri);
            var relativePath = Uri.UnescapeDataString(relativeUri.ToString()).Replace('/', Path.DirectorySeparatorChar);
            return relativePath;
        }
    }
}