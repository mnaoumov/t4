﻿using System.Text;
using Microsoft.VisualStudio.TextTemplating;

namespace ConsoleApplication2
{
    public abstract class DesignTimeTemplateBase : TextTransformation, ITemplate
    {
        StringBuilder ITemplate.GenerationEnvironment
        {
            get { return GenerationEnvironment; }
            set { GenerationEnvironment = value; }
        }
    }
}