﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;

namespace ConsoleApplication2
{
    public abstract class RunTimeTemplateBase : ITemplate
    {
        readonly ToStringInstanceHelper _toStringHelper = new ToStringInstanceHelper();
        string _currentIndent = "";
        bool _endsWithNewline;
        CompilerErrorCollection _errors;
        StringBuilder _generationEnvironment;
        List<int> _indentLengths;
        ITemplate _parent;

        /// <summary>
        /// The string builder that generation-time code is using to assemble generated output
        /// </summary>
        protected StringBuilder GenerationEnvironment
        {
            get
            {
                if (_parent != null)
                    return _parent.GenerationEnvironment;
                if (_generationEnvironment == null)
                    _generationEnvironment = new StringBuilder();
                return _generationEnvironment;
            }
            set
            {
                if (_parent != null)
                    _parent.GenerationEnvironment = value;
                else
                    _generationEnvironment = value;
            }
        }

        /// <summary>
        /// The error collection for the generation process
        /// </summary>
        public CompilerErrorCollection Errors
        {
            get
            {
                if (_errors == null)
                    _errors = new CompilerErrorCollection();
                return _errors;
            }
        }

        /// <summary>
        /// A list of the lengths of each indent that was added with PushIndent
        /// </summary>
        List<int> indentLengths
        {
            get
            {
                if (_indentLengths == null)
                    _indentLengths = new List<int>();
                return _indentLengths;
            }
        }

        /// <summary>
        /// Gets the current indent we use when adding lines to the output
        /// </summary>
        public string CurrentIndent
        {
            get { return _currentIndent; }
        }

        /// <summary>
        /// Current transformation session
        /// </summary>
        public virtual IDictionary<string, object> Session { get; set; }

        public ToStringInstanceHelper ToStringHelper
        {
            get { return _toStringHelper; }
        }

        public virtual string TransformText()
        {
            return GenerationEnvironment.ToString();
        }

        /// <summary>
        /// Write text directly into the generated output
        /// </summary>
        public void Write(string textToAppend)
        {
            if (string.IsNullOrEmpty(textToAppend))
                return;
            // If we're starting off, or if the previous text ended with a newline,
            // we have to append the current indent first.
            if (GenerationEnvironment.Length == 0 || _endsWithNewline)
            {
                GenerationEnvironment.Append(_currentIndent);
                _endsWithNewline = false;
            }
            // Check if the current text ends with a newline
            if (textToAppend.EndsWith(Environment.NewLine, StringComparison.CurrentCulture))
                _endsWithNewline = true;
            // This is an optimization. If the current indent is "", then we don't have to do any
            // of the more complex stuff further down.
            if (_currentIndent.Length == 0)
            {
                GenerationEnvironment.Append(textToAppend);
                return;
            }
            // Everywhere there is a newline in the text, add an indent after it
            textToAppend = textToAppend.Replace(Environment.NewLine, Environment.NewLine + _currentIndent);
            // If the text ends with a newline, then we should strip off the indent added at the very end
            // because the appropriate indent will be added when the next time Write() is called
            if (_endsWithNewline)
                GenerationEnvironment.Append(textToAppend, 0, textToAppend.Length - _currentIndent.Length);
            else
                GenerationEnvironment.Append(textToAppend);
        }

        /// <summary>
        /// Write text directly into the generated output
        /// </summary>
        public void WriteLine(string textToAppend)
        {
            Write(textToAppend);
            GenerationEnvironment.AppendLine();
            _endsWithNewline = true;
        }

        /// <summary>
        /// Write formatted text directly into the generated output
        /// </summary>
        public void Write(string format, params object[] args)
        {
            Write(string.Format(CultureInfo.CurrentCulture, format, args));
        }

        /// <summary>
        /// Write formatted text directly into the generated output
        /// </summary>
        public void WriteLine(string format, params object[] args)
        {
            WriteLine(string.Format(CultureInfo.CurrentCulture, format, args));
        }

        /// <summary>
        /// Raise an error
        /// </summary>
        public void Error(string message)
        {
            Errors.Add(new CompilerError { ErrorText = message });
        }

        /// <summary>
        /// Raise a warning
        /// </summary>
        public void Warning(string message)
        {
            Errors.Add(new CompilerError { ErrorText = message, IsWarning = true });
        }

        /// <summary>
        /// Increase the indent
        /// </summary>
        public void PushIndent(string indent)
        {
            if (indent == null)
                throw new ArgumentNullException("indent");
            _currentIndent += indent;
            indentLengths.Add(indent.Length);
        }

        /// <summary>
        /// Remove the last indent that was added with PushIndent
        /// </summary>
        public string PopIndent()
        {
            string returnValue = "";
            if (indentLengths.Count > 0)
            {
                int indentLength = indentLengths[indentLengths.Count - 1];
                indentLengths.RemoveAt(indentLengths.Count - 1);
                if ((indentLength > 0))
                {
                    returnValue = _currentIndent.Substring(_currentIndent.Length - indentLength);
                    _currentIndent = _currentIndent.Remove(_currentIndent.Length - indentLength);
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Remove any indentation
        /// </summary>
        public void ClearIndent()
        {
            indentLengths.Clear();
            _currentIndent = "";
        }

        protected virtual void GenerateToFile(string fileName, Action action)
        {
            var filePath = GetAbsoluteFilePath(fileName);
            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            var previousEnvironment = GenerationEnvironment;
            GenerationEnvironment = new StringBuilder();
            try
            {
                action();
                File.WriteAllText(filePath, GenerationEnvironment.ToString(), Encoding.UTF8);
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Generation of file {0} failed.", filePath), e);
            }
            finally
            {
                GenerationEnvironment = previousEnvironment;
            }
        }

        protected string GetAbsoluteFilePath(string fileName)
        {
            if (!Path.IsPathRooted(fileName))
                fileName = Path.Combine(OutputDirectory, fileName);
            return fileName;
        }

        public string OutputDirectory { get; set; }

        protected void WriteWithoutIndent(string textToAppend)
        {
            var currentIndent = _currentIndent;
            _currentIndent = "";
            Write(textToAppend);
            _currentIndent = currentIndent;

        }

        /// <summary>
        /// Utility class to produce culture-oriented representation of an object as a string.
        /// </summary>
        public class ToStringInstanceHelper
        {
            IFormatProvider _formatProvider = CultureInfo.InvariantCulture;
            static readonly Dictionary<Type, Func<object, IFormatProvider, string>> _toStringFuncs = new Dictionary<Type, Func<object, IFormatProvider, string>>();

            /// <summary>
            /// Gets or sets format provider to be used by ToStringWithCulture method.
            /// </summary>
            public IFormatProvider FormatProvider
            {
                get { return _formatProvider; }
                set
                {
                    if (value != null)
                        _formatProvider = value;
                }
            }

            /// <summary>
            /// This is called from the compile/run appdomain to convert objects within an expression block to a string
            /// </summary>
            public string ToStringWithCulture(object objectToConvert)
            {
                if (objectToConvert == null)
                    return null;

                Type type = objectToConvert.GetType();
                MethodInfo method = type.GetMethod("ToString", new[] { typeof(IFormatProvider) });
                string result;
                if (method == null)
                    result = objectToConvert.ToString();
                else
                    result = (string) (method.Invoke(objectToConvert, new object[] { FormatProvider }));
                return result;
            }
        }

        public void SetParent(ITemplate parent)
        {
            _parent = parent;
        }

        StringBuilder ITemplate.GenerationEnvironment
        {
            get { return GenerationEnvironment; }
            set { GenerationEnvironment = value; }
        }
    }
}