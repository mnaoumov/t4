using Microsoft.VisualStudio.TextTemplating;

namespace T4Helper
{
    public class ToStringHelperWrapper
    {
        public string ToStringWithCulture(object objectToConvert)
        {
            return ToStringHelper.ToStringWithCulture(objectToConvert);
        }
    }
}