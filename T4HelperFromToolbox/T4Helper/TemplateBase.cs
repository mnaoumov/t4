﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using T4Toolbox;

namespace T4Helper
{
    public abstract class TemplateBase
    {
        static readonly ToStringHelperWrapper _toStringHelperWrapper = new ToStringHelperWrapper();

        static TemplateBase()
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            AppDomain.CurrentDomain.AssemblyResolve += (o, args) => ResolveAssembly(executingAssembly, args.Name);
        }

        static Assembly ResolveAssembly(Assembly executingAssembly, string unresolvedAssemblyName)
        {
            var resourceFileName = string.Format("{0}.dll", new AssemblyName(unresolvedAssemblyName).Name);
            using (Stream stream = executingAssembly.GetManifestResourceStream(typeof(TemplateBase), resourceFileName))
            {
                if (stream == null)
                    return null;
                using (var memoryStream = new MemoryStream())
                {
                    stream.CopyTo(memoryStream);
                    byte[] rawAssembly = memoryStream.ToArray();
                    Assembly loadedAssembly = Assembly.Load(rawAssembly);
                    return loadedAssembly;
                }
            }
        }

        public ToStringHelperWrapper ToStringHelper
        {
            get { return _toStringHelperWrapper; }
        }

        public abstract string TransformText();

        readonly IFullT4ToolboxTemplateApi _proxy;

        protected TemplateBase()
        {
            _proxy = T4ToolboxTemplate.CreateProxy(new TemplateBaseProxy(this));
        }

        protected virtual void OnRendering(EventArgs e)
        {
            _proxy.OnRendering(e);
        }

        protected virtual void Validate()
        {
            _proxy.Validate();
        }

        public virtual void Initialize()
        {
            _proxy.Initialize();
        }

        protected virtual void Dispose(bool disposing)
        {
            _proxy.Dispose(disposing);
        }

        public virtual IDictionary<string, object> Session
        {
            get { return _proxy.Session; }
            set { _proxy.Session = value; }
        }

        protected StringBuilder GenerationEnvironment
        {
            get { return _proxy.GenerationEnvironment; }
            set { _proxy.GenerationEnvironment = value; }
        }

        public string CurrentIndent
        {
            get { return _proxy.CurrentIndent; }
        }

        public bool Enabled
        {
            get { return _proxy.Enabled; }
            set { _proxy.Enabled = value; }
        }

        public CompilerErrorCollection Errors
        {
            get { return _proxy.Errors; }
        }

        public OutputInfo Output
        {
            get { return _proxy.Output; }
        }

        public void Dispose()
        {
            _proxy.Dispose();
        }

        public void Write(string textToAppend)
        {
            _proxy.Write(textToAppend);
        }

        public void WriteLine(string textToAppend)
        {
            _proxy.WriteLine(textToAppend);
        }

        public void Write(string format, params object[] args)
        {
            _proxy.Write(format, args);
        }

        public void WriteLine(string format, params object[] args)
        {
            _proxy.WriteLine(format, args);
        }

        public void Error(string message)
        {
            _proxy.Error(message);
        }

        public void Warning(string message)
        {
            _proxy.Warning(message);
        }

        public void PushIndent(string indent)
        {
            _proxy.PushIndent(indent);
        }

        public string PopIndent()
        {
            return _proxy.PopIndent();
        }

        public void ClearIndent()
        {
            _proxy.ClearIndent();
        }

        public void Error(string format, params object[] args)
        {
            _proxy.Error(format, args);
        }

        public void Render()
        {
            _proxy.Render();
        }

        public void RenderToFile(string fileName)
        {
            _proxy.RenderToFile(fileName);
        }

        public string Transform()
        {
            return _proxy.Transform();
        }

        public void Warning(string format, params object[] args)
        {
            _proxy.Warning(format, args);
        }

        public event EventHandler Rendering
        {
            add { _proxy.Rendering += value; }
            remove { _proxy.Rendering -= value; }
        }

        class TemplateBaseProxy : IOverridableToolboxTemplateApi
        {
            readonly TemplateBase _templateBase;

            public TemplateBaseProxy(TemplateBase templateBase)
            {
                _templateBase = templateBase;
            }

            public string TransformText()
            {
                return _templateBase.TransformText();
            }

            public void OnRendering(EventArgs e)
            {
                _templateBase.OnRendering(e);
            }

            public void Validate()
            {
                _templateBase.Validate();
            }

            public void Initialize()
            {
                _templateBase.Initialize();
            }

            public void Dispose(bool disposing)
            {
                _templateBase.Dispose(disposing);
            }

            public IDictionary<string, object> Session
            {
                get { return _templateBase.Session; }
                set { _templateBase.Session = value; }
            }
        }
    }
}