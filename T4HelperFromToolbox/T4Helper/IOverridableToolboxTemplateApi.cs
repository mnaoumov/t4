using System;
using System.Collections.Generic;

namespace T4Helper
{
    public interface IOverridableToolboxTemplateApi
    {
        void OnRendering(EventArgs e);
        void Validate();
        string TransformText();
        void Initialize();
        void Dispose(bool disposing);
        IDictionary<string, object> Session { get; set; }
    }
}