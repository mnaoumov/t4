using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Text;
using T4Toolbox;

namespace T4Helper
{
    class T4ToolboxTemplate : Template, IOverridableToolboxTemplateApi
    {
        protected override void OnRendering(EventArgs e)
        {
            _templateBaseProxy.OnRendering(e);
        }

        protected override void Validate()
        {
            _templateBaseProxy.Validate();
        }

        public override string TransformText()
        {
            return _templateBaseProxy.TransformText();
        }

        public override void Initialize()
        {
            _templateBaseProxy.Initialize();
        }

        protected override void Dispose(bool disposing)
        {
            _templateBaseProxy.Dispose(disposing);
        }

        public override IDictionary<string, object> Session
        {
            get { return _templateBaseProxy.Session; }
            set { _templateBaseProxy.Session = value; }
        }

        readonly IOverridableToolboxTemplateApi _templateBaseProxy;

        T4ToolboxTemplate(IOverridableToolboxTemplateApi templateBaseProxy)
        {
            _templateBaseProxy = templateBaseProxy;
        }

        void IOverridableToolboxTemplateApi.OnRendering(EventArgs e)
        {
            base.OnRendering(e);
        }

        void IOverridableToolboxTemplateApi.Validate()
        {
            base.Validate();
        }

        void IOverridableToolboxTemplateApi.Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public static IFullT4ToolboxTemplateApi CreateProxy(IOverridableToolboxTemplateApi templateBaseProxy)
        {
            return new T4ToolboxTemplateProxy(new T4ToolboxTemplate(templateBaseProxy));
        }

        class T4ToolboxTemplateProxy : IFullT4ToolboxTemplateApi
        {
            T4ToolboxTemplate T4ToolboxTemplate { get; set; }
            IOverridableToolboxTemplateApi BaseApi { get { return T4ToolboxTemplate; } }

            public T4ToolboxTemplateProxy(T4ToolboxTemplate t4ToolboxTemplate)
            {
                T4ToolboxTemplate = t4ToolboxTemplate;
            }

            public void OnRendering(EventArgs e)
            {
                BaseApi.OnRendering(e);
            }

            public void Validate()
            {
                BaseApi.Validate();
            }

            public string TransformText()
            {
                return BaseApi.TransformText();
            }

            public void Initialize()
            {
                BaseApi.Initialize();
            }

            public void Dispose(bool disposing)
            {
                BaseApi.Dispose(disposing);
            }

            public IDictionary<string, object> Session
            {
                get { return BaseApi.Session; }
                set { BaseApi.Session = value; }
            }

            public void Dispose()
            {
                T4ToolboxTemplate.Dispose();
            }

            public void Write(string textToAppend)
            {
                T4ToolboxTemplate.Write(textToAppend);
            }

            public void WriteLine(string textToAppend)
            {
                T4ToolboxTemplate.WriteLine(textToAppend);
            }

            public void Write(string format, params object[] args)
            {
                T4ToolboxTemplate.Write(format, args);
            }

            public void WriteLine(string format, params object[] args)
            {
                T4ToolboxTemplate.WriteLine(format, args);
            }

            public void Error(string message)
            {
                T4ToolboxTemplate.Error(message);
            }

            public void Warning(string message)
            {
                T4ToolboxTemplate.Warning(message);
            }

            public void PushIndent(string indent)
            {
                T4ToolboxTemplate.PushIndent(indent);
            }

            public string PopIndent()
            {
                return T4ToolboxTemplate.PopIndent();
            }

            public void ClearIndent()
            {
                T4ToolboxTemplate.ClearIndent();
            }

            public StringBuilder GenerationEnvironment
            {
                get { return T4ToolboxTemplate.GenerationEnvironment; }
                set { T4ToolboxTemplate.GenerationEnvironment = value; }
            }

            public string CurrentIndent
            {
                get { return T4ToolboxTemplate.CurrentIndent; }
            }

            public void Error(string format, params object[] args)
            {
                T4ToolboxTemplate.Error(format, args);
            }

            public void Render()
            {
                T4ToolboxTemplate.Render();
            }

            public void RenderToFile(string fileName)
            {
                T4ToolboxTemplate.RenderToFile(fileName);
            }

            public string Transform()
            {
                return T4ToolboxTemplate.Transform();
            }

            public void Warning(string format, params object[] args)
            {
                T4ToolboxTemplate.Warning(format, args);
            }

            public bool Enabled
            {
                get { return T4ToolboxTemplate.Enabled; }
                set { T4ToolboxTemplate.Enabled = value; }
            }

            public CompilerErrorCollection Errors
            {
                get { return T4ToolboxTemplate.Errors; }
            }

            public OutputInfo Output
            {
                get { return T4ToolboxTemplate.Output; }
            }

            public event EventHandler Rendering
            {
                add { T4ToolboxTemplate.Rendering += value; }
                remove { T4ToolboxTemplate.Rendering -= value; }
            }
        }
    }
}