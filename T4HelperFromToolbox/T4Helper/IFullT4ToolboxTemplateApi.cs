using System;
using System.CodeDom.Compiler;
using System.Text;
using T4Toolbox;

namespace T4Helper
{
    public interface IFullT4ToolboxTemplateApi : IOverridableToolboxTemplateApi
    {
        StringBuilder GenerationEnvironment { get; set; }
        string CurrentIndent { get; }
        bool Enabled { get; set; }
        CompilerErrorCollection Errors { get; }
        OutputInfo Output { get; }
        void Dispose();
        void Write(string textToAppend);
        void WriteLine(string textToAppend);
        void Write(string format, params object[] args);
        void WriteLine(string format, params object[] args);
        void Error(string message);
        void Warning(string message);
        void PushIndent(string indent);
        string PopIndent();
        void ClearIndent();
        void Error(string format, params object[] args);
        void Render();
        void RenderToFile(string fileName);
        string Transform();
        void Warning(string format, params object[] args);
        event EventHandler Rendering;
    }
}