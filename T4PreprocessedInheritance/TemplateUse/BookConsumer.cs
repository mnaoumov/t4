﻿namespace TemplateUse
{
    using System;

    /// <summary>
    /// A class to carry data about a book in a library system.
    /// </summary>
    public class Book
    {
        /// <summary>
        /// The title of the book.
        /// </summary>
        public string Title { get; set; }
    
        /// <summary>
        /// The ID of the author of the book.
        /// </summary>
        public string AuthorID { get; set; }
    
        /// <summary>
        /// The standard ISBN number of the book.
        /// </summary>
        public string ISBN { get; set; }
    
        /// <summary>
        /// The number of copies the library owns.
        /// </summary>
        public int CopiesOwned { get; set; }
    
    }

}