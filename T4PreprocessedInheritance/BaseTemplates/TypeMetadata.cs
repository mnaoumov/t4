﻿// Copyright (C) Microsoft Corporation.  All rights reserved.
using System.Collections.Generic;

namespace BaseTemplates
{
    // Structures to partially describe a data structure - these are deliberately simplistic to keep the example small and general
    // They could be reused across a number of templates generating database tables, simple UI forms, data carrier classes, etc.
    public class TypeDescription
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<TypePropertyDescription> Properties = new List<TypePropertyDescription>();
    }

    public class TypePropertyDescription
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    }

}
